#+TITLE:  老子33, lao tzu: tao de ching 33
#+STARTUP: showall
#+OPTIONS: toc:nil num:nil timestamp:nil
#+HTML_HEAD: <style> @page { size: 210mm 297mm; margin: 4mm; margin-bottom: 6mm; } </style>
#+HTML_HEAD: <style> h1.title { display: none; }  </style>
#+HTML_HEAD: <style> h1, h2, h3, h4 { font-weight: 500; font-size: 1.2em; }  </style>
#+HTML_HEAD: <style> div#postamble { display: none; }  </style>

#+HTML_HEAD: <style> h2, h3 { margin: 8px 0; }  </style>
#+HTML_HEAD: <style> p { text-indent: .5em; }  </style>
#+HTML_HEAD: <style> p, ul { margin: 3px 2px; }  </style>
#+HTML_HEAD: <style> a { color: grey; text-decoration: none; font-size: 7pt; }  </style>

#+HTML_HEAD: <style> h2 { display: none; }  </style>
#+HTML_HEAD: <style> div.figure { margin: 0; padding: 0; }   </style>
#+HTML_HEAD: <style>  img { width: 10mm; float: right;}  </style>
#+HTML_HEAD: <style>  h2, h3  { float: left; font-weight: 400; font-size: 10pt;}  </style>
#+HTML_HEAD: <style>  h3 { margin: .1em; margin-right: 1em; } </style>
# #+HTML_HEAD: <style> div.outline-2 { border-bottom: thin solid black; } </style>
#+HTML_HEAD: <style> div.outline-3 { clear: right; }   </style>
#+HTML_HEAD: <style> p, li { margin: .1em; font-size: 9pt; }  </style>

#+HTML_HEAD: <style> table { clear: left; font-size: 9pt;  }  </style>
#+HTML_HEAD: <style> td.org-left { margin-bottom: 0; padding-bottom: 0; }  </style>
#+HTML_HEAD: <style> td:nth-child(3) { font-size: 8pt;  }  </style>
# #text-org2ed575b > table:nth-child(2) > tbody:nth-child(2) > tr:nth-child(1) > td:nth-child(3)


* ChonMage site
** 老子33： 原文 と 書き下し文 と Old Translation by James Legge
[[file:R33-ChonMage.png]]
| 知人者智、     | 人を知る者は智、                               | He who knows other men is discerning;                                     |
| 自知者明。     | 自ら知る者は明(めい)なり。                     | he who knows himself is intelligent.                                      |
| 勝人者有力、   | 人に勝つ者は力有り、                           | He who overcomes others is strong;                                        |
| 自勝者強。     | 自ら勝つ者は強し。                             | he who overcomes himself is mighty.                                       |
| 知足者富、     | 足るを知る者は富み、                           | He who is satisfied with his lot is rich;                                 |
| 強行者有志。   | 強(つと)めて行なう者は志有り。                 | he who goes on acting with energy has a (firm) will.                      |
| 不失其所者久。 | その所を失わざる者は久し。                     | He who does not fail in the requirements of his position, continues long; |
| 死而不亡者壽。 | 死して而(しか)も亡びざる者は寿(いのちなが)し。 | he who dies and yet does not perish, has longevity.                       |
[[file:Tao-Teh-King-Legge-trans-gtnbrg.png]]
https://blog.mage8.com/roushi-33 , 
https://www.gutenberg.org/ebooks/216

** 老子33： Jane English Chapter 33 と へいはちろう
| Knowing others is wisdom;                            | To understand others is ordinary wisdom.                                                   |
| Knowing the self is enlightenment.                   | But to understand oneself is clear wisdom.                                                 |
| Mastering others requires force;                     | You need power to defeat others.                                                           |
| Mastering the self needs strength.                   | But you need true power to defeat yourself.                                                |
| He who knows he has enough is rich.                  | A person who knows contentment has true wealth.                                            |
| Perseverance is a sign of will power.                | A person who continues his efforts has already achieved his purpose.                       |
| He who stays where he is endures.                    | A person who keeps his original self can last long time.                                   |
| To die but not to perish is to be eternally present. | To accept one’s plain self along “the way” and to forget about death is true longevity. |
[[file:R33-JaneEnglish.png]]
https://www.wussu.com/laotzu/laotzu33.html

* AOki Kenji
** 老子33； 福永光司 と  Ursula K. Le Guin 33
[[file:R33-AokiKenji.png]]
| 他人を知るものは智者であるが、                         | Knowing other people is intelligence,        |
| 己れを知るものは明者である。                           | knowing yourself is wisdom.                  |
| 他人に勝つものは力をもつが、                           | Overcoming others takes strength,            |
| 己れに勝つものは真の強者である。                       | overcoming yourself takes greatnenss.        |
| 己れに足ることを知るものは富み、                       | Contentment is wealth.                       |
| 道に努め励むものは向上心をもつ。                       | Boldly pushing forward takes resolution.     |
| 己れにふさわしい在り方を失わなわぬものは永つづきがし、 | Staying put keeps you in position.           |
| 死んでも朽ち果てないのを永遠に生きるという。           | To live till you die is to live long enough. |
[[file:Ursula-LeGuin-page.png]]
https://www.ursulakleguin.com/lao-tzu-the-tao-te-ching

* Ch44
** 老子44 , Tao Te Ching Ch44 
| 名與身孰親。     | Fame or self: Which matters more?                           | 名声と生命（いのち）とは、どちらが切実であろうか。                               |
| 身與貨孰多。     | Self or wealth: Which is more precious?                     | 生命と財貨とは、どちらが大切であろうか。                                         |
| 得與亡孰病。     | Gain or loss; Which is more painful?                        | わがものにするのと失くするのとは、どちらが苦痛であろうか。                       |
| 是故甚愛必大費。 | He who is attached to things will suffer much.              | だから、ひどく外物（がいぶつ）に執着すれば、すっかり生命をすりへらす羽目となり、 |
| 多蔵必厚亡。     | He who saves will suffer heavy loss.                        | しこたま貯めこむと、ごっそり持ってゆかれること必定。                             |
| 知足不辱。       | A contented man is never disappointed.                      | 満足することを知れば、辱めをうけることもなく、                                   |
| 知止不殆。       | He who knows when to stop does not find himself in trouble. | ふみ止まることを知れば、危うい目にあうこともない。                               |
| 可以長久。       | He will stay for ever safe.                                 | いつまでもやすらかでいられるのだ。                                               |
https://aokikenji.com/tao-44 , 
https://www.wussu.com/laotzu/laotzu44.html
