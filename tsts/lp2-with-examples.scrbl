#lang scribble/lp2
@(require scribble/base
          scribble/example)

@; https://lists.racket-lang.org/users/archive/2014-December/065188.html

@title{An Example}

This would be a @bold{wonderful} way to accomplish things!


@chunk[<*>
        (define (f x)
          <f-body>)]

@chunk[<f-body>
        (* x x)]


And then, I could show an example:

@(begin
   (require syntax/location)
   (define here (quote-source-file))
   (define evaluator (make-base-eval))
   (evaluator `(begin
                (dynamic-require '(file ,here) #f)
                (current-namespace
                 (module->namespace '(file ,here))))))

@examples[
        #:eval evaluator
        (f 10)
]
