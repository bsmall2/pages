#lang scribble/lp2
@(require scribble/base
          scribble/example
	  scriblib/footnote
	  racket/file
	  (for-label racket/file racket/list sxml) )

@title{Learning Racket with SXML}
@(define-footnote a-fn generate-footnotes)	  
@(begin
   (require syntax/location)
   (define here (quote-source-file))
   (define evaluator (make-base-eval))
   (evaluator `(begin
                (dynamic-require '(file ,here) #f)
                (current-namespace
                 (module->namespace '(file ,here))))))
@; I might as well keep make-base-eval since I'll have to
@; ; add racket/file and other procedures to lp2 anyway
@; ; https://docs.racket-lang.org/scribble/eval.html#%28def._%28%28lib._scribble%2Feval..rkt%29._make-base-eval%29%29
@; avoid list of requires with (make-evaluator 'racket)... or learn where procedure code is??

I think Oleg Kiselyov made XML and XPath usable with Scheme.@a-fn{
Oleg Kiselyov's pages: @; } ok!
  @hyperlink["https://okmij.org/ftp/scheme/sxml-short-paper.html" #:underline? #f]{SXML Short Paper},
  @hyperlink["https://okmij.org/ftp/papers/sxs-talk.pdf" #:underline? #f]{SXs Talk: XML, XPATH, and XSLT to SXML, SXPATH, and SXSLT}
} Examples from his site are a way to explore Racket's documentation and approach for sxml.@a-fn{
Racket Documentation: @hyperlink["https://docs.racket-lang.org/sxml/index.html"]{sxml}, @hyperlink["https://docs.racket-lang.org/sxml/ssax.htm"]{ssax: for xml->sxml srl:sxml->xml...}, @hyperlink["https://docs.racket-lang.org/sxml/sxpath"]{sxpath}, @hyperlink["https://docs.racket-lang.org/html-parsing/index.html"]{html-parsing: for webscraping}
@; TODO: need hyperlink yasnippet
}


@chunk[<requires>
 (require racket/list racket/file
 	  sxml)
]

With @racket{#lang racket} at the top of a script there is no need to @racket{require} @code{racket/list} for @racket{first} or @code{racket/file} for @racket{file->string}. But since this is a re-working of procedures explored in a variety or scripts for educational purposes,@a-fn{
Exploratory floundering at: @hyperlink["https://codeberg.org/bsmall2/Teacher-Racket/src/branch/main/webscraping/Xpath"]{Codeberg repository: Teacher-Racket}}
The use of @racket{#lang scribble/lp2} and @racket{make-base-eval} will make explicit the location of procedure that are not defined for @racket{#lang racket/base}.

This is what the xml file looks like:
@(define xml-string (file->string "data/examples-weather.xml"))

@verbatim[@xml-string] @; either type of brackets work! {}, []


@chunk[<defines>
(define tree-weather
  (ssax:xml->sxml (open-input-file "data/examples-weather.xml") '()))
]

This is what sxml looks like:
@examples[ #:eval evaluator

	tree-weather

]

SXML works as scheme data and code so we can take out parts for more manageable views.
@examples[ #:eval evaluator
        
	(first tree-weather)
]

SXPath works to find and show any part of the tree.

@examples[ #:eval evaluator

	((sxpath '(// |@| Title)) tree-weather)

]


@chunk[<*>
     <requires>
     <defines>

]

@generate-footnotes[]
